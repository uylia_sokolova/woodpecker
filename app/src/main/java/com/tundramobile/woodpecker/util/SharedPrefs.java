package com.tundramobile.woodpecker.util;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by yuliasokolova on 18.08.16.
 */
public class SharedPrefs {

    public static final String USER_ID         = "UserID";
    public static final String PHONE_NUMBER    = "phone_no";
    public static final String COUNTRY_CODE    = "country_code";
    public static final String COUNTRY_NAME    = "country_name";
    public static final String USER_INFO_SP    = "user_info";
    public static final String FB_CREDENTIALS  = "fb_credentials";
    public static final String TOKEN           = "fb_access_token";
    public static final String EXPIRES         = "fb_expires_in";
    public static final String FRIENDS         = "fb_friendList";
    public static final String FB_ID           = "fb_friendList";
    public static final String FB_EMAIL        = "fb_email";
    public static final String FB_NAME         = "fb_name";

    public static  void setUserInfo(Context context, String user_id, String phone, String countryName, String countryCode){
        SharedPreferences sharedPref = context.getSharedPreferences(USER_INFO_SP, context.MODE_PRIVATE);
        SharedPreferences.Editor editorPref = sharedPref.edit();

        if (user_id != null)
        editorPref.putString(USER_ID,  user_id);

        editorPref.putString(PHONE_NUMBER,   phone);
        editorPref.putString(COUNTRY_NAME, countryName);
        editorPref.putString(COUNTRY_CODE, countryCode);
        editorPref.commit();
    }
    public static boolean saveFbCredentials(Context context,String token, long expires, String friendList) {
        SharedPreferences.Editor editor = context.getSharedPreferences(FB_CREDENTIALS, Context.MODE_PRIVATE).edit();
        editor.putString(TOKEN, token);
        editor.putLong(EXPIRES, expires);
        editor.putString(FRIENDS, friendList);
        return editor.commit();
    }
    public static boolean saveFbCredentials(Context context,String id,  String email, String name) {
        SharedPreferences.Editor editor = context.getSharedPreferences(FB_CREDENTIALS, Context.MODE_PRIVATE).edit();
        editor.putString(FB_ID, id);
        editor.putString(FB_EMAIL, email);
        editor.putString(FB_NAME, name);
        return editor.commit();
    }
    public static void saveUserId (Context context, String userId){
        SharedPreferences sharedPref = context.getSharedPreferences(SharedPrefs.USER_INFO_SP, context.MODE_PRIVATE);
        sharedPref.edit().putString(SharedPrefs.USER_ID, userId).commit();
    }

}
