package com.tundramobile.woodpecker.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.ui.UI;

/**
 * Created by yuliasokolova on 16.08.16.
 */
public class NetworkConnection {
    ConnectivityManager connectivityManager;
    boolean connected = false;

    public boolean haveNetworkConnection(Context context) {
        try {
            connectivityManager = (ConnectivityManager) context.getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnectedOrConnecting();
            if (!connected) UI.showToast(Constants.ERROR_NO_NETWORK_CONNECTION, context);
            return connected;
        } catch (Exception e) {
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
}

