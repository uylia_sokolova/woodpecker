package com.tundramobile.woodpecker.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by yuliasokolova on 05.08.16.
 */
public class Fonts {

    Context context;
    private Typeface font_raleway_semiBold;
    private Typeface font_raleway_medium;
    private Typeface font_raleway_regular;
    private Typeface font_roboto_regular;
    private Typeface font_roboto_bold;

    public Fonts(Context context) {
        this.context = context;
        font_raleway_medium = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Medium.ttf");
        font_raleway_regular = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
        font_raleway_semiBold = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-SemiBold.ttf");
        font_roboto_regular = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoRegular.ttf");
        font_roboto_bold = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoBold.ttf");
    }

    // main bold font
    public void setRalewayMedium(ViewGroup view){
        setFont(view, font_raleway_medium);
    }

    // main
    public void setRalewayRegular(ViewGroup view){
        setFont(view, font_raleway_regular);
    }

    // font for title in login screen "Woodpecker"
    public void setRalewaySemibold(TextView view){
        view.setTypeface(font_raleway_semiBold);
    }

    public void setRalewaySemibold(ViewGroup view){
        setFont(view, font_raleway_semiBold);
    }

   //toolbar, tabs,  choosen item in spinner
    public void setRobotoBold(ViewGroup view){
        setFont(view, font_roboto_bold);
    }

   //dialog
    public void setRobotoRegular(ViewGroup view){
        setFont(view, font_roboto_regular);
    }

    private void setFont(ViewGroup view, Typeface typeface) {
        for (int i = 0; i < view.getChildCount(); i++){
            View v = view.getChildAt(i);
            if(v instanceof TextView) ((TextView)v).setTypeface(typeface);
               else if (v instanceof Button) ((Button)v).setTypeface(typeface);
                    else if(v instanceof ViewGroup) setFont((ViewGroup) v, typeface);
        }
    }

    public Typeface getFont_raleway_semiBold() {
        return font_raleway_semiBold;
    }

    public Typeface getFont_raleway_medium() {
        return font_raleway_medium;
    }

    public Typeface getFont_raleway_regular() {
        return font_raleway_regular;
    }

    public Typeface getFont_roboto_regular() {
        return font_roboto_regular;
    }

    public Typeface getFont_roboto_bold() {
        return font_roboto_bold;
    }

}
