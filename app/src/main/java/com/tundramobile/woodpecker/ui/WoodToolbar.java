package com.tundramobile.woodpecker.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tundramobile.woodpecker.R;
import com.tundramobile.woodpecker.util.Fonts;


/**
 * Created by yuliasokolova on 18.08.16.
 */
public class WoodToolbar extends LinearLayout{

    private TextView            title;
    private ImageView           leftImage;
    private ImageView           fbIcon, infoIcon;
    private ImageView           rightImage;
    private Fonts               fontUtil;

    public WoodToolbar(Context context, AttributeSet attributes) {
        super(context, attributes);
        LinearLayout toolbar = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.actionbar, null);
        addView(toolbar);

        title = (TextView) toolbar.findViewById(R.id.screen_title);
        leftImage = (ImageView) toolbar.findViewById(R.id.left_button);
        rightImage = (ImageView) toolbar.findViewById(R.id.right_button);

        fbIcon = (ImageView) toolbar.findViewById(R.id.fb_icon);
        infoIcon= (ImageView) toolbar.findViewById(R.id.info_icon);

        fontUtil = new Fonts(context);
        title.setTypeface(fontUtil.getFont_roboto_bold());
    }
    public void setTitleToolbar (String title_text){
        title.setText(title_text);
    }

    public void setBackBtn (OnClickListener listener){
        leftImage.setImageResource(R.drawable.back_btn);
        leftImage.setOnClickListener(listener);
    }
    public void setSettingsBtn (OnClickListener listener) {
        leftImage.setImageResource(R.drawable.icon_setting);
        leftImage.setOnClickListener(listener);
    }
    public void setInviteBtn (OnClickListener listener){
        rightImage.setOnClickListener(listener);
    }
    public void setUserInfoBtn (OnClickListener listener_info, OnClickListener listener_fb){
        fbIcon.setVisibility(VISIBLE);
        infoIcon.setVisibility(VISIBLE);
        infoIcon.setOnClickListener(listener_info);
        fbIcon.setOnClickListener(listener_fb);
    }

    public ImageView getLeftImage() {
        return leftImage;
    }

    public ImageView getRightImage() {
        return rightImage;
    }
}
