package com.tundramobile.woodpecker.modals;

import java.io.Serializable;

public class FriendInfo implements Serializable {
	private String image;
	private String name;
	private String id;
	private String phone_no;
	private String unread_count;
	private String lastseen;
	//private String userId;

	public String getLastseen() {
		return lastseen;
	}

	public void setLastseen(String lastseen) {
		this.lastseen = lastseen;
	}

//	public String getUserId()
//	{
//		return userId;
//	}
//
//	public void setUserId(String userId)
//	{
//		this.userId = userId;
//	}

	public String getUnread_count()
	{
		return unread_count;
	}

	public void setUnread_count(String unread_count)
	{
		this.unread_count = unread_count;
	}

	public String getPhone_no()
	{
		return phone_no;
	}

	public void setMobileNumber(String mobile_no)
	{
		phone_no = mobile_no;
	}

	public String getImage()
	{
		return image;
	}

	public void setImage(String image) {
	image = image;
}

	public String getName() {
	return name;
}

	public void setName(String name) {
	this.name = name;
}

	public String getId() {
	return id;
}

	public void setId(String id) {
	this.id = id;
}




}
