package com.tundramobile.woodpecker.modals;


import com.tundramobile.woodpecker.server.BaseService;

import org.json.JSONObject;

/**
 * Created by yuliasokolova on 16.08.16.
 */
public class RequestResult {
    public boolean isOk; //if result is successfull or not
    public int code; // http response code
    public String method; // method associated with this request
    public String error; // error message if any
    public String fullResult; // json response string
    public JSONObject data;
    public BaseService.Operation operation;
}
