//package com.tundramobile.woodpecker.activity;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.SharedPreferences.Editor;
//import android.os.AsyncTask;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.view.Gravity;
//import android.view.KeyEvent;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.LinearInterpolator;
//import android.view.animation.RotateAnimation;
//import android.widget.EditText;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.app.util.GlobalUtills;
//import com.app.util.UI;
//import com.app.webserviceshandler.SetLastSeen;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdView;
//import com.tundramobile.woodpecker.R;
//import com.tundramobile.woodpecker.fragment.AroundMeFragment;
//import com.tundramobile.woodpecker.fragment.ChatFragment;
//import com.tundramobile.woodpecker.fragment.FragmentMainMenu;
//import com.tundramobile.woodpecker.fragment.GroupsFragment;
//import com.tundramobile.woodpecker.fragment.HangoutFragment;
//import com.tundramobile.woodpecker.fragment.WorldGroupFragment;
//import com.tundramobile.woodpecker.ui.UI;
//import com.tundramobile.woodpecker.ui.WoodToolbar;
//import com.tundramobile.woodpecker.util.NetworkConnection;
//
//public class MainMenu extends FragmentActivity {
//    private static String user_id  = "";
//
//    SharedPreferences sharedPref;
//    Editor            editorPref;
//    MenuAdapter         adapter;
//    TabLayout tabLayout;
//    ViewPager pager;
//    View tabView;
//    TextView title_tab;
//    ImageView image_tab;
//    WoodToolbar toolbar;
//    RelativeLayout removeText_;
//    EditText searchEditText_;
//    View[] tab_view_list = new View[5];
//
//    NetworkConnection networkConnection;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.activity_main_menu);
//
//        pager = (ViewPager) findViewById(R.id.main_pager);
//        tabLayout = (TabLayout) findViewById(R.id.main_menu_tabs);
//        searchEditText_  = (EditText)       findViewById(R.id.search_edit_text);
//        removeText_      = (RelativeLayout) findViewById(R.id.layout_cencel_img);
//        toolbar = (WoodToolbar) findViewById(R.id.toolbar_main_menu);
//
//        networkConnection = new NetworkConnection();
//        initNewGroupButton();
//
//        initSettingsLayout();
//
//        setPagerAndTabs();
//        setGoogleAdv();
//
//        sharedPref = getSharedPreferences("login", MODE_PRIVATE);
//        editorPref = sharedPref.edit();
//
////        todo implement search
////            removeText_.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                searchEditText_.setText("");
////
////                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
////
////                imm.hideSoftInputFromWindow(searchEditText_.getWindowToken(), 0);
////
////                getWorldGroupList_ = new GettingWorldGroupList();
////
////                getWorldGroupList_.execute("");
////
////                removeText_.setVisibility(View.INVISIBLE);
////            }
////        });
////
////        searchEditText_.addTextChangedListener(new TextWatcher() {
////            @Override
////            public void onTextChanged(CharSequence s, int start, int before, int count) {
////                removeText_.setVisibility(View.VISIBLE);
////            }
////
////            @Override
////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
////            }
////
////            @Override
////            public void afterTextChanged(Editable s) {
////            }
////        });
////
////        searchEditText_.setOnEditorActionListener(new TextView.OnEditorActionListener()
////        {
////            @Override
////            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
////            {
////                removeText_.setVisibility(View.VISIBLE);
////
////                if (actionId == EditorInfo.IME_ACTION_SEARCH)
////                {
////                    if (!NetworkCheck.getConnectivityStatusString(getActivity()).equalsIgnoreCase("true"))
////                        NetworkCheck.openInternetDialog(getActivity());
////
////                    else
////                    {
////                        searchText_ = searchEditText_.getText().toString().trim();
////
////                        if (searchText_.length() <= 0)
////                            GlobalUtills.showToast("Please enter a keyword..!", getActivity());
////
////                        else
////                        {
////                            InputMethodManager imm =
////                                    (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
////
////                            imm.hideSoftInputFromWindow(searchEditText_.getWindowToken(), 0);
////
////                            new GettingWorldGroupList().execute(searchText_);
////                        }
////                    }
////                }
////
////                return false;
////            }
////        });
//
//
////        if (sharedPref.contains("notification_flag_single_chat"))
////        {
////            try
////            {
////                GlobalUtills.msgCountSingle = sharedPref.getString("msgcount", "1");
////
////                GlobalUtills.badge1.setText("" + GlobalUtills.msgCountSingle);
////                GlobalUtills.badge1.toggle ();
////
////                editorPref.remove("notification_flag_single_chat");
////                editorPref.commit();
////            }
////            catch (Exception e)
////            {
////                e.printStackTrace();
////            }
////        }
////
////        if (sharedPref.contains("notification_flag_mychat"))
////        {
////            try
////            {
////                GlobalUtills.msgCountGroup = sharedPref.getString("msgcountG", "1");
////
////                GlobalUtills.badgeGroup.setText("" + GlobalUtills.msgCountGroup);
////                GlobalUtills.badgeGroup.toggle ();
////
////                editorPref.remove("notification_flag_mychat");
////                editorPref.commit();
////            }
////            catch (Exception e)
////            {
////                e.printStackTrace();
////            }
////        }
////
////        if (sharedPref.contains("notification_flag_adgroup"))
////        {
////            editorPref.remove("notification_flag_adgroup");
////            editorPref.commit();
////        }
////
////        if (sharedPref.contains("notification_flag_add_menber"))
////        {
////            editorPref.remove("notification_flag_add_menber");
////            editorPref.commit();
////        }
////
////        if (sharedPref.contains("notification_flag_approve_request"))
////        {
////            editorPref.remove("notification_flag_approve_request");
////            editorPref.commit();
////        }
////
////        if (sharedPref.contains("notification_flag_join_gorup"))
////        {
////            editorPref.remove("notification_flag_join_gorup");
////            editorPref.commit();
////        }
////
////        if (sharedPref.contains("notification_flag_approve_join_gorup"))
////        {
////            editorPref.remove("notification_flag_approve_join_gorup");
////            editorPref.commit();
////        }
//
//
//
//    }
//
//    private void initSettingsLayout() {
//        toolbar.leftImage().setImageResource(R.drawable.icon_setting);
//        toolbar.leftImage().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f,
//                        Animation.RELATIVE_TO_SELF, .5f);
//
//                anim.setInterpolator(new LinearInterpolator());
//                anim.setRepeatCount(1);
//                anim.setDuration(1700);  // TODO:
//
//                toolbar.leftImage().setAnimation(anim);
//                toolbar.leftImage().startAnimation(anim);
//
//                new CountDownTimer(600, 600) {
//                    @Override
//                    public void onTick(long millisUntilFinished) {
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        Intent intent = new Intent(MainMenu.this, SettingActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                    }
//                }.start();
//            }
//        });
//    }
//
//
//    private void setGoogleAdv() {
//        AdView adView    = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);
//    }
//
//    private View initTabView(){
//        tabView = View.inflate(this, R.layout.menu_tab_view, null);
//        title_tab = (TextView) tabView.findViewById(R.id.tv_tab_menu);
//        image_tab = (ImageView) tabView.findViewById(R.id.iv_tab_menu);
//        return tabView;
//    }
//
//    private void setTabs(){
//        for (int i = 0; i < 5; i++) {
//            tabLayout.addTab(tabLayout.newTab().setTag(i));
//        }
//    }
//
//    private void setPagerAndTabs() {
//
//        adapter = new MenuAdapter(getSupportFragmentManager());
//        pager.setAdapter(adapter);
//        pager.setCurrentItem(0);
//        setTabs();
//        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                tabLayout.getTabAt(position).select();
//                adapter.selectIcon(position);
//                searchEditText_.setText("");
//                removeText_.setVisibility(View.INVISIBLE);
//                toolbar.setTitleToolbar(adapter.getCurrentFragment(position).getFragmentTitle());
//
//                if (position == 4) {
//                    toolbar.getRightImage().setVisibility(View.VISIBLE);
//                    toolbar.setInviteBtn(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            String shareBody = getResources().getString(R.string.invite);
//                            String shareSubject = getResources().getString(R.string.app_name);
//                            String shareTitle = getResources().getString(R.string.title_share);
//
//                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            sharingIntent.setType("text/plain");
//                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSubject);
//                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//                            startActivity(Intent.createChooser(sharingIntent, shareTitle));
//                        }
//                    });
//                }else
//                    toolbar.getRightImage().setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//            }
//        });
//
//        for (int i = 0; i < tabLayout.getTabCount(); i++) {
//            TabLayout.Tab tab = tabLayout.getTabAt(i);
//            tab_view_list[i] = adapter.getTabView(i);
//            tab.setCustomView(tab_view_list[i]);
//        }
//
//        adapter.selectIcon(0);
//
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                int position = (int) tab.getTag();
//                if (pager.getCurrentItem() != position) {
//                    pager.setCurrentItem(position);
//                    adapter.selectIcon(position);
//                    searchEditText_.setText("");
//                    removeText_.setVisibility(View.INVISIBLE);
//                    UI.hideKeyboard(MainMenu.this);
//                }
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//                adapter.unSelectIcon((int)tab.getTag());
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//        });
//    }
//
//    private void initNewGroupButton (){
//        (new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainMenu.this, AddGroupActivity.class);
//                startActivity(intent);
//            }
//        });
//    }
//
//    public void setToolbarListener(){
//        searchEditText_.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (networkConnection.haveNetworkConnection(MainMenu.this)) {
//                    adapter.getCurrentFragment(pager.getCurrentItem()).startSearch(getFilter());
//                    return true;
//                } else
//                    return false;
//            }
//        });
//    }
//
//    public String getFilter(){
//        return searchEditText_.getText().toString().trim();
//    }
//
//    @Override
//    protected void onDestroy() {
//        ChatOneToOne.setShareImage("");
//        SetALastSeen(user_id, "N");
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onStop() {
//        SetALastSeen(user_id, "N");
//        super.onStop();
//    }
//
//
//
//    private void SetALastSeen(String user_id,String lastseen) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
//            new SetLastSeen(user_id,lastseen).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        else
//            new SetLastSeen(user_id,lastseen).execute();
//    }
//
//
//    public void showNoResults(FrameLayout view, String text) {
//        TextView msg = new TextView(MainMenu.this);
//        msg.setText(text);
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        msg.setLayoutParams(params);
//        msg.setGravity(Gravity.CENTER);
//        view.addView(msg);
//    }
//
//
//
//    private class  MenuAdapter extends FragmentPagerAdapter {
//        View tab_view;
//        FragmentMainMenu[] pages = new FragmentMainMenu[5];
//        public MenuAdapter (FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            switch (position){
//                case 0:
//                    pages[0] = WorldGroupFragment.getInstance();
//                    break;
//                case 1:
//                    pages[1]= GroupsFragment.getInstance();
//                    break;
//                case 2:
//                    pages[2] = AroundMeFragment.getInstance();
//                    break;
//                case 3:
//                    pages[3] = HangoutFragment.getInstance();
//                    break;
//                case 4:
//                    pages[4] = ChatFragment.getInstance();
//                    break;
//                default:
//                    pages[0] = WorldGroupFragment.getInstance();
//            }
//            return pages[position];
//        }
//
//        public FragmentMainMenu getCurrentFragment (int pos){
//            return pages[pos];
//        }
//
//        private String tabTitles[] = new String[] { getResources().getString(R.string.tab0),
//                                                    getResources().getString(R.string.tab1),
//                                                    getResources().getString(R.string.tab2),
//                                                    getResources().getString(R.string.tab3),
//                                                    getResources().getString(R.string.tab4)};
//        private int[] imageResId = { R.drawable.groups_icon,
//                                     R.drawable.my_groups_icon,
//                                     R.drawable.around_me_icon,
//                                     R.drawable.hangouts_icon,
//                                     R.drawable.chat_icon};
//
//        private int[] imageResIdSelected = { R.drawable.groups_icon_selected,
//                                             R.drawable.my_groups_selected,
//                                             R.drawable.around_me_selected_icon,
//                                             R.drawable.hangouts_selected_icon,
//                                             R.drawable.chat_selected_icon};
//        public View getTabView(int position) {
//            initTabView();
//            title_tab.setText(tabTitles[position]);
//            image_tab.setImageResource(imageResId[position]);
//            return tabView;
//        }
//
//        public void selectIcon (int position){
//            tab_view = tab_view_list[position];
//            ((ImageView)tab_view.findViewById(R.id.iv_tab_menu)).setImageResource(imageResIdSelected[position]);
//            ((TextView)tab_view.findViewById(R.id.tv_tab_menu)).setTextColor(getResources().getColor(R.color.white));
//        }
//
//        public void unSelectIcon (int position){
//            tab_view = tab_view_list[position];
//            ((ImageView)tab_view.findViewById(R.id.iv_tab_menu)).setImageResource(imageResId[position]);
//            ((TextView)tab_view.findViewById(R.id.tv_tab_menu)).setTextColor(getResources().getColor(R.color.black));
//        }
//
//        @Override
//        public int getCount() {
//            return pages.length;
//        }
//    }
//
//}
