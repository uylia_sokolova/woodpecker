package com.tundramobile.woodpecker.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.tundramobile.woodpecker.R;
import com.tundramobile.woodpecker.modals.FriendInfo;
import com.tundramobile.woodpecker.receiver.IncomingSms;
import com.tundramobile.woodpecker.server.FbFriendService;
import com.tundramobile.woodpecker.server.FbRegService;
import com.tundramobile.woodpecker.util.Fonts;
import com.tundramobile.woodpecker.util.NetworkConnection;
import com.tundramobile.woodpecker.util.SharedPrefs;
import com.tundramobile.woodpecker.ui.UI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by yuliasokolova on 17.08.16.
 */
public class LoginFacebook extends Activity {

    private LoginButton               loginButton;
    private LinearLayout              gmailLogin;
    private LinearLayout              facebookLogin;
    private CallbackManager           callbackManager;

    private String fbToken;
    private long   fbExpir;
    private String fbFrstName;
    private String fbId;
    private String fbEmail;
    private String fbEmailFromServer;
    private String friendList = "";

    ArrayList<FriendInfo> friendFbList   = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login_fb);

        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();

        if (isLoggedIn())
         //   startActivity(new Intent(LoginFacebook.this, MainMenu.class));
            UI.showToast("go to main menu", LoginFacebook.this);

        fbEmailFromServer = getIntent().getStringExtra(SharedPrefs.FB_EMAIL);
        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.setReadPermissions(Arrays.asList(new String[]{"email", "public_profile",
                "read_friendlists", "user_friends"}));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                fbToken = loginResult.getAccessToken().getToken();
                fbExpir = loginResult.getAccessToken().getExpires().getTime();
                fbId = loginResult.getAccessToken().getUserId();

                getMyFbCredentials(loginResult);
            }

            @Override
            public void onCancel() {
                UI.showToast("Login was cancelled", LoginFacebook.this);
            }

            @Override
            public void onError(FacebookException error) {
                UI.showToast(error.getMessage(), LoginFacebook.this);
            }
        });

        findViewById(R.id.fb_login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });

        setFonts();
    }

    private void setFonts(){
        Fonts fontUtil = new Fonts(this);
        fontUtil.setRalewaySemibold((TextView) findViewById(R.id.woodpecker));
        fontUtil.setRalewaySemibold((LinearLayout)findViewById(R.id.fb_login_btn));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void saveFbCredentials(String token, long expires, String friendList) {
        SharedPrefs.saveFbCredentials(LoginFacebook.this, token, expires, friendList);
    }

    public void saveFbCredentials(String id,  String email, String name) {
        SharedPrefs.saveFbCredentials(LoginFacebook.this, id, email, name);
    }

    private void getMyFbCredentials(final LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.v("LoginActivity", response.toString() + " json " + object);
                        try {
                            fbEmail = object.getString("email");
                            fbFrstName = object.getString("name");
                            confirmPhoneAndEmail(loginResult);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getFbFriends(LoginResult loginResult) {
        GraphRequest friendRequest = GraphRequest.newMyFriendsRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONArrayCallback() {
                    public void onCompleted(JSONArray object, GraphResponse response) {
                        try {
                            friendList = response.getJSONObject().getString("data").toString();
                            Log.d("FB FRIENDS: ", friendList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        saveFbCredentials(fbToken, fbExpir, friendList);
                        callFbRegistration();

                    }
                });

        Bundle params = new Bundle();
        params.putString("fields", "id,name,email,picture,gender");
        friendRequest.setParameters(params);
        friendRequest.executeAsync();
    }

    private String getFriendsId(String friendList) {
        String fbIdCheckValidation = "";
        try {
            JSONArray jsonArray  = new JSONArray(friendList);
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String id = obj.getString("id");
                if (fbIdCheckValidation.equals(""))
                    fbIdCheckValidation = id + "";
                else
                    fbIdCheckValidation = fbIdCheckValidation + "," + id;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fbIdCheckValidation;
    }

    private void callFriendRequest(String friendsId) {
        if (new NetworkConnection().haveNetworkConnection(LoginFacebook.this))
            new FbFriendService(LoginFacebook.this, new FbFriendService.FriendServiceCallback() {
            @Override
            public void onGetFriends(ArrayList<FriendInfo> friendList) {
                friendFbList = friendList;
             //   startActivity(new Intent(LoginFacebook.this, MainMenu.class));
                UI.showToast("go to main menu", LoginFacebook.this);
            }
        }).getFbFriend(friendsId);
    }


    private void confirmPhoneAndEmail(LoginResult loginResult) {
        String inputPhone = getSharedPreferences(SharedPrefs.USER_INFO_SP, MODE_PRIVATE).getString(SharedPrefs.PHONE_NUMBER, "");
//       if (!fbEmail.equals(fbEmailFromServer)) {
//            UI.showToast(fbEmail + " is not associated with the " + inputPhone + " number.", LoginFacebook.this);
//            startActivity(new Intent(LoginFacebook.this, PhoneCountryConfirm.class));
//            finish();
//        }else {
            saveFbCredentials(fbId, fbEmail, fbFrstName);
            getFbFriends(loginResult);
//        }
    }

    private void callFbRegistration() {
        String firstName, facebookId, email, country, phoneNumber, userId;
        SharedPreferences sp_user = getSharedPreferences(SharedPrefs.USER_INFO_SP, MODE_PRIVATE);
        phoneNumber = sp_user.getString(SharedPrefs.PHONE_NUMBER, "");
        userId = sp_user.getString(SharedPrefs.USER_ID, "");
        country = sp_user.getString(SharedPrefs.COUNTRY_NAME, "");

        SharedPreferences  sp_fb = getSharedPreferences(SharedPrefs.FB_CREDENTIALS, Context.MODE_PRIVATE);
        firstName = sp_fb.getString(SharedPrefs.FB_NAME, "");
        facebookId = sp_fb.getString(SharedPrefs.FB_ID, "");
        email = sp_fb.getString(SharedPrefs.FB_EMAIL, "");


        new FbRegService(LoginFacebook.this, new FbRegService.FbServiceCallback() {
            @Override
            public void onGetFbReg(boolean isOk) {
                if (isOk)
                    callFriendRequest(getFriendsId(friendList));
                else
                    UI.showToast("Fail", LoginFacebook.this);
            }
        }).getFbRegistration(firstName, country, facebookId, email, phoneNumber, userId);

    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

}
