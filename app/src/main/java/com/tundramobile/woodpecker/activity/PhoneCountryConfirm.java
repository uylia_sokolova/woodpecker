package com.tundramobile.woodpecker.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.tundramobile.woodpecker.adapter.SpinnerGreenAdapter;
import com.tundramobile.woodpecker.modals.CountryInfo;
import com.tundramobile.woodpecker.receiver.GCMHelper;
import com.tundramobile.woodpecker.server.LoginService;
import com.tundramobile.woodpecker.util.NetworkConnection;
import com.tundramobile.woodpecker.util.SharedPrefs;
import com.tundramobile.woodpecker.ui.UI;
import com.tundramobile.woodpecker.R;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.util.Fonts;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by yuliasokolova on 16.08.16.
 */
public class PhoneCountryConfirm extends Activity implements LoginService.LoginServiceCallback {

    private EditText                  txtCountryCode;
    private EditText                  txtPhoneNumber;
    private ProgressBar               pr_bar;
    private Button                    buttonLogin;
    private Spinner                   spnCountryName;
    private ImageView                 imgViewCountryName;
    private String                    googleRegId        = "";
    private Intent                    intent             = null;
    private Bundle                    bundle             = new Bundle();
    private List<String>              listCountryName    = new ArrayList<String>();
    private String                    countryName        = "";
    private String                    countryCode        = "";
    private String                    regId              = "";
    private String                    verificationCode   = "";
    private String                    userId             = "";
    private String                    email              = "";
    private int                       countryId          = 0;
    private Fonts                     fontUtil;
    private NetworkConnection         networkConnection  = null;
    private LoginService              loginService;
    List<CountryInfo>	              country_code_list	 = new ArrayList<>();
    String					          country_iso_code	 = "IN";
    CountryInfo                       countryInfo;

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_phone_country_confirm);

        txtCountryCode      = (EditText)    findViewById(R.id.text_view_country_code);
        txtPhoneNumber      = (EditText)    findViewById(R.id.edit_text_phone_number);
        buttonLogin         = (Button)      findViewById(R.id.btn_login_phone_register);
        spnCountryName      = (Spinner)     findViewById(R.id.spn_country_name);
        imgViewCountryName  = (ImageView)   findViewById(R.id.img_view_spn_country_name);
        pr_bar              = (ProgressBar) findViewById(R.id.pr_bar_login);

        networkConnection = new NetworkConnection();
        loginService = new LoginService(PhoneCountryConfirm.this, this);

        countryInfo = new CountryInfo(this);
        countryInfo.initCountryCode();
        country_code_list = countryInfo.parseCountryCode();

        checkPermission();
        setFonts();

        imgViewCountryName.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                spnCountryName.performClick();
            }
        });

        buttonLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pr_bar.setVisibility(View.VISIBLE);

                if (txtPhoneNumber.getText().toString().equals(""))
                    UI.showToast("Please Enter Your Phone No.", PhoneCountryConfirm.this);

                if (txtPhoneNumber.getText().toString().length() < 7)
                    UI.showToast("Phone number should be minimum 7 digits.", PhoneCountryConfirm.this);

                if (!networkConnection.haveNetworkConnection(PhoneCountryConfirm.this))
                    UI.showToast(Constants.ERROR_NO_NETWORK_CONNECTION, PhoneCountryConfirm.this);

                new GCMHelper(PhoneCountryConfirm.this, new GCMHelper.OnGetGoogleRegNumber() {
                     @Override
                     public void onGetRegistration(String googleRegId) {
                         if (!googleRegId.toString().equals(""))
                            callLoginApi(txtPhoneNumber.getText().toString());
                         else
                             UI.showToast("Please try again.", PhoneCountryConfirm.this);
                         }
                    }).registerUser();
            }
        });

        TelephonyManager tm = (TelephonyManager) getSystemService(PhoneCountryConfirm.this.TELEPHONY_SERVICE);
        countryName                  = tm.getSimCountryIso();
        country_iso_code = countryName;

        if (country_iso_code.equals("")) country_iso_code = "IL";

        for (int i = 0; i < country_code_list.size(); ++i) {
            listCountryName.add(country_code_list.get(i).getName());

            if (countryName.equalsIgnoreCase(country_code_list.get(i).getShort_name())) {
                countryCode = country_code_list.get(i).getCountry_code();
                countryId   = country_code_list.get(i).getCountry_id  ();
                txtCountryCode.setText(countryCode);
            }
        }

        SpinnerGreenAdapter adapter = new SpinnerGreenAdapter(this, R.layout.spinner_layout, listCountryName,
                fontUtil.getFont_roboto_bold(), fontUtil.getFont_roboto_regular(), spnCountryName);
        adapter.setDropDownViewResource(R.layout.spinner_layout_dropdown);
        spnCountryName.setAdapter(adapter);

        for (int i = 0; i < listCountryName.size(); ++i) {
            if (countryName.equalsIgnoreCase(country_code_list.get(i).getShort_name()))
                spnCountryName.setSelection(i);
        }

        spnCountryName.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View v, int position, long id) {
                countryCode = country_code_list.get(position).getCountry_code();
                countryId   = country_code_list.get(position).getCountry_id();
                txtCountryCode.setText(countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });
    }

    private void callLoginApi(String phoneNumber) {
        if (phoneNumber.startsWith("0"))
            phoneNumber = phoneNumber.substring(1);
            loginService.getUserId(String.valueOf(countryId), googleRegId, phoneNumber);
    }

    @Override
    public void onGetUserId(String user_id, String email) {
        pr_bar.setVisibility(View.GONE);
        saveUser(user_id, txtPhoneNumber.getText().toString(), countryName, countryCode);
        UI.showToast("user_id=" + user_id, PhoneCountryConfirm.this);

        intent = new Intent(PhoneCountryConfirm.this, LoginFacebook.class);
        intent.putExtra(SharedPrefs.FB_EMAIL, email);
        startActivity(intent);
        finish();
    }

    @Override
    public void onGetUserCode(String regId, String securId) {
        pr_bar.setVisibility(View.GONE);
        saveUser(null, txtPhoneNumber.getText().toString(), countryName, countryCode);
        UI.showToast("securId=" + securId, PhoneCountryConfirm.this);
        intent  = new Intent(PhoneCountryConfirm.this, CodeVerificationScreen.class);
        bundle.putString(CodeVerificationScreen.REGISTER_ID, regId);
        bundle.putString(CodeVerificationScreen.VERIFICATION_CODE, securId);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    private void saveUser(String user_id, String phone, String countryName, String countryCode){
        SharedPrefs.setUserInfo(PhoneCountryConfirm.this, user_id, phone, countryName, countryCode);
    }

    private void setFonts() {
        fontUtil = new Fonts(this);
        fontUtil.setRalewaySemibold((TextView) findViewById(R.id.woodpecker));
        fontUtil.setRalewaySemibold(buttonLogin);
        ((TextView)findViewById(R.id.signin_confirm_country)).setTypeface(fontUtil.getFont_raleway_medium());
        ((TextView)findViewById(R.id.signin_phone_no)).setTypeface(fontUtil.getFont_raleway_medium());
        txtCountryCode.setTypeface(fontUtil.getFont_roboto_regular());
        txtPhoneNumber.setTypeface(fontUtil.getFont_roboto_regular());
    }

    private void checkPermission (){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED   ) {
                requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS){
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED)
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
                    showDialogOK("SMS Permission required for this app",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkPermission();
                                            dialog.dismiss();
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            dialog.dismiss();
                                            break;
                                    }
                                }
                            });
                }

        }
    }
    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }





}
