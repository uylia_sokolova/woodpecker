package com.tundramobile.woodpecker.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tundramobile.woodpecker.R;
import com.tundramobile.woodpecker.receiver.IncomingSms;
import com.tundramobile.woodpecker.server.CodeVerificationService;
import com.tundramobile.woodpecker.util.Fonts;
import com.tundramobile.woodpecker.ui.UI;


/**
 * Created by yuliasokolova on 17.08.16.
 */
public class CodeVerificationScreen extends Activity {

    private Button         buttonComplete;
    private Bundle         bundle            = new Bundle();
    private Intent         intent            = null;
    private ProgressBar    pr_bar;
    private String         registrationID    = "";
    private EditText       et_code;
    private TextView       tvResend;
    private BroadcastReceiver receiver;
    private IntentFilter   filter;
    public static String REGISTER_ID         = "register_id";
    public static String VERIFICATION_CODE   = "verification_code";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_verification);
        UI.hideKeyboard(this);

        bundle         = getIntent().getExtras();
        registrationID = bundle.getString(REGISTER_ID);

        et_code = (EditText) findViewById(R.id.txt_verification_code);
        buttonComplete = (Button) findViewById(R.id.btn_code_verify_phone_register);
        pr_bar  = (ProgressBar) findViewById(R.id.pr_bar_verification);

        tvResend = (TextView) findViewById(R.id.txtSendSecure);
        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        buttonComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CodeVerificationService(CodeVerificationScreen.this, new CodeVerificationService.CodeServiceCallback() {
                    @Override
                    public void verificationIsDone(boolean isOk) {
                        if (!isOk)
                            UI.showToast("verification is fail", CodeVerificationScreen.this);

                        intent = new Intent(CodeVerificationScreen.this, LoginFacebook.class);
                        startActivity(intent);
                        finish();
                    }
                }).getUserVerification(registrationID, et_code.getText().toString());
            }
        });
        initSmsReceiver();
        setFonts();
    }

    private void initSmsReceiver (){
        receiver = new BroadcastReceiver() {
            String verify_code;
            @Override
            public void onReceive(Context context, Intent intent) {
                verify_code = new IncomingSms().getVerifyCode(intent);
                et_code.setText(verify_code);
            }
        };
        filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        if (receiver != null)
            unregisterReceiver(receiver);

        super.onDestroy();
    }


    private void setFonts() {
        Fonts fontUtil = new Fonts(this);
        fontUtil.setRalewaySemibold((TextView) findViewById(R.id.woodpecker));
        fontUtil.setRalewaySemibold(buttonComplete);
        fontUtil.setRalewayMedium((LinearLayout) findViewById(R.id.tv_confirm_screen));
    }
}
