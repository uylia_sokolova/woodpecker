package com.tundramobile.woodpecker.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yuliasokolova on 04.08.16.
 */
public class SpinnerGreenAdapter extends ArrayAdapter<String> {
    Typeface choosen_item, dropdown_item;
    Spinner spinner;

        public SpinnerGreenAdapter(Context context, int textViewResourceId,
                                   List<String> objects, Typeface choosen_item, Typeface dropdown_item, Spinner spinner) {
            super(context, textViewResourceId, objects);
            this.choosen_item = choosen_item;
            this.dropdown_item = dropdown_item;
            this.spinner = spinner;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            if (view instanceof TextView)  ((TextView) view).setTypeface(dropdown_item);

            return view;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            convertView =  super.getDropDownView(position, convertView, parent);

            if (spinner.getSelectedItemPosition() == position)
                ((TextView) convertView).setTypeface(choosen_item);
            else
                ((TextView) convertView).setTypeface(dropdown_item);

            return convertView;
        }
    }

