package com.tundramobile.woodpecker.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.AdapterView;

import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.ui.UI;

/**
 * Created by yuliasokolova on 16.08.16.
 */
public class IncomingSms{
    Bundle bundle;
    SmsMessage[] msgs = null;
    SmsMessage message;
    String sender_number;
    String msg_text;
    String verify_code;

    public String getVerifyCode (Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED"))
        bundle = intent.getExtras();
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdusObj.length];
                for (int i = 0; i < msgs.length; i++) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        String format  = bundle.getString("format");
                        message = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
                    } else
                        message = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                        sender_number  = message.getDisplayOriginatingAddress();
                        msg_text       = message.getDisplayMessageBody();
                        verify_code    = msg_text.substring(msg_text.length() - 4, msg_text.length());
                }
            }
        Log.d("SMS", "mes " + message + "body " + msg_text);
        return verify_code;
        }
    }
