package com.tundramobile.woodpecker.server;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.modals.RequestResult;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by eugenetroyanskii on 01.06.16.
 */
public class ResponceHandler extends AsyncHttpResponseHandler {
    Context context;
    String method;
    RequestCallback callback;
    BaseService.Operation operation;

    public static final String SUCCESS = "Success";
    public static final String MESSAGE = "Message";

    public ResponceHandler(Context context, String method, String filter,
                           RequestCallback callback, BaseService.Operation operation) {
        this.operation = operation;
        this.context = context;
        this.method = method;
        this.callback = callback;
    }
    public ResponceHandler(Context context, RequestCallback callback, BaseService.Operation operation) {
        this.operation = operation;
        this.context = context;
        this.callback = callback;
    }
    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//        for (Header h : headers) {
//            if (h.getName().equals("Set-Cookie")){
//                cookie = h.getValue().substring(0, h.getValue().indexOf(";"));
//            }
//        }
        if (callback == null) {
            return;
        }

        Log.d(BaseService.TAG, new String(responseBody));
        RequestResult res = new RequestResult();
        res.code = statusCode;
        res.method = method;
        res.isOk = statusCode == 200;
        res.operation = operation;
        try {
            JSONObject jsonObj = new JSONObject(new String(responseBody));
            String message = jsonObj.getString(MESSAGE);
            if (message.equalsIgnoreCase(SUCCESS)){
                res.fullResult = new String(responseBody);
                res.data = jsonObj;
            }else res.isOk = false;
        }catch (JSONException e){
            res.error = e.getMessage();
            e.printStackTrace();
        }

        if (responseBody == null)
            if (res.error == null || res.error.isEmpty())
                res.error = "No data";

        callback.onRequestResult(res);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        error.printStackTrace();
        Log.e(BaseService.TAG, "api error " + method);
        Log.e(BaseService.TAG, "code " + statusCode);
        if (responseBody != null)
            Log.e(BaseService.TAG, "body " + new String(responseBody));
        Log.e(BaseService.TAG, error.getMessage(), error);

        if (callback == null) {
            return;
        }

        RequestResult res = new RequestResult();
        res.code = statusCode;
        res.method = method;
        res.isOk = false;
        res.error = error.getMessage();

        callback.onRequestResult(res);
    }



}