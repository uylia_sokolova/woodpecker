package com.tundramobile.woodpecker.server;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.modals.FriendInfo;
import com.tundramobile.woodpecker.modals.RequestResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by yuliasokolova on 17.08.16.
 */
public class FbFriendService extends BaseService implements RequestCallback  {
    FriendServiceCallback callback;
    ArrayList<FriendInfo> friendList;

    public FbFriendService(Context context, FriendServiceCallback callback) {
        super(context);
        setCallback(this);
        this.callback = callback;
    }

    public void getFbFriend (String facebookIdCheckValidation){
        RequestParams params = new RequestParams();
        params.put("post_type", "post");
        params.put("mtype", "get_valid_fb_users");
        params.put("users", facebookIdCheckValidation);

        get(Constants.URL, params);

    }

    @Override
    public void onRequestResult(RequestResult result) {
        if (result.isOk) {
            JSONArray jsonArr = null;
            FriendInfo friend;
            JSONObject jsonFriend;
            friendList = new ArrayList<>();
            friend = new FriendInfo();

            try{
                jsonArr = result.data.getJSONArray("users");

                for (int i = 0; i < jsonArr.length(); ++i) {
                    jsonFriend = jsonArr.getJSONObject(i);
                    friend.setId       (jsonFriend.getString("facebook_id"));
                    friend.setName     (jsonFriend.getString("user_name"));
                    friend.setMobileNumber(jsonFriend.getString("user_telephone"));
                    friendList.add(friend);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
            callback.onGetFriends(friendList);
        }
    }

    public interface FriendServiceCallback {
        void onGetFriends(ArrayList<FriendInfo> friendList);
        }
}
