package com.tundramobile.woodpecker.server;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.tundramobile.woodpecker.util.NetworkConnection;
import com.tundramobile.woodpecker.ui.UI;

import org.json.JSONObject;

/**
 * Created by yuliasokolova on 16.08.16.
 */
public class BaseService {
    Context context;
    AsyncHttpClient httpClient;
    RequestCallback callback;
    NetworkConnection networkConnection;

    public static final String TAG = "SERVICE";

    public enum Operation {
        GET,POST,PUT,DELETE,
    }

    public BaseService(Context context) {
        this.context = context;
        networkConnection = new NetworkConnection();
        httpClient = new AsyncHttpClient();
        httpClient.setTimeout(30000);
    }

    public void setCallback(RequestCallback callback) {
        this.callback = callback;
    }

    public void get(String url, RequestParams params) {
        if(networkConnection.haveNetworkConnection(context)) {
            log(url, null);
            httpClient.get(context, url, params,
                    new ResponceHandler(context, callback, Operation.GET));
        } else {
            UI.showToast("Please, check network connection", context);
        }
    }

    private void log(String url, JSONObject params) {
        Log.i(TAG, "calling " + url);
        if (params != null)
            Log.i(TAG, params.toString());
    }
}
