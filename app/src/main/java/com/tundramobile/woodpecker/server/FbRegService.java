package com.tundramobile.woodpecker.server;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.modals.RequestResult;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuliasokolova on 17.08.16.
 */
public class FbRegService extends BaseService implements RequestCallback  {
    FbServiceCallback callback;
    public FbRegService(Context context, FbServiceCallback callback) {
        super(context);
        setCallback(this);
        this.callback = callback;
    }

    public void getFbRegistration (String firstName, String country, String facebookId, String email,
                           String phoneNumber, String userId){
        RequestParams params = new RequestParams();
        params.put("post_type", "post");
        params.put("mtype", "register");
        params.put("uname", firstName);
        params.put("ucountry", country);
        params.put("facebook_id", facebookId);
        params.put("uemail", email);
        params.put("ph", phoneNumber);
        params.put("issocial", "Y");
        params.put("userId", userId);

        get(Constants.URL, params);
    }

    @Override
    public void onRequestResult(RequestResult result) {
        if (result.isOk)
            callback.onGetFbReg(true);
        else
            callback.onGetFbReg(false);
    }

    public interface FbServiceCallback {
        void onGetFbReg(boolean isOk);
        }

}
