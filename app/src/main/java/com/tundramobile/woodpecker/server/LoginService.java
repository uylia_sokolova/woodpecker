package com.tundramobile.woodpecker.server;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.RequestParams;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.modals.RequestResult;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuliasokolova on 17.08.16.
 */
public class LoginService extends BaseService implements RequestCallback  {
    LoginServiceCallback callback;
    public LoginService(Context context, LoginServiceCallback callback) {
        super(context);
        setCallback(this);
        this.callback = callback;
    }

    public void getUserId (String countryId, String googleRegId, String phoneNumber){
        RequestParams params = new RequestParams();
        params.put("post_type", "post");
        params.put("mtype", "registerup");
        params.put("country_id", countryId);
        params.put("device_id", googleRegId);
        params.put("phone_no", phoneNumber);
        params.put("devicetype", "A");

        get(Constants.URL, params);
    }

    @Override
    public void onRequestResult(RequestResult result) {
        if (result.isOk) {
            String regId   = "";
            String securId = "";
            String userId  = "";
            String email   = "";

            try{
                JSONObject jsonObject = new JSONObject(result.fullResult);
                if (jsonObject.has("user_id")) {
                    userId  = jsonObject.getString("user_id");
                    email   = jsonObject.getString("user_email");
                    callback.onGetUserId(userId, email);
                }
                else{
                    securId = jsonObject.getString("secure_code");
                    regId = jsonObject.getString("register_id");
                    callback.onGetUserCode(regId, securId);
                }

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public interface LoginServiceCallback {
        void onGetUserId(String user_id, String email);
        void onGetUserCode( String regId, String securId);
        }

}
