package com.tundramobile.woodpecker.server;

import com.tundramobile.woodpecker.modals.RequestResult;

/**
 * Created by yuliasokolova on 16.08.16.
 */
public interface RequestCallback {
    void onRequestResult(RequestResult result);

}