package com.tundramobile.woodpecker.server;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.tundramobile.woodpecker.constants.Constants;
import com.tundramobile.woodpecker.modals.RequestResult;
import com.tundramobile.woodpecker.util.SharedPrefs;
import com.tundramobile.woodpecker.ui.UI;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yuliasokolova on 17.08.16.
 */
public class CodeVerificationService extends BaseService implements RequestCallback  {
    CodeServiceCallback callback;

    public CodeVerificationService(Context context, CodeServiceCallback callback) {
        super(context);
        setCallback(this);
        this.callback = callback;
    }

    public void getUserVerification (String registrationId, String secureCode){
        RequestParams params = new RequestParams();
        params.put("post_type", "post");
        params.put("mtype", "verify_user");
        params.put("registration_id", registrationId);
        params.put("secure_code", secureCode);

        get(Constants.URL, params);

    }

    @Override
    public void onRequestResult(RequestResult result) {
        if (result.isOk) { // // FIXME: 18.08.16 messge Failure with status ok
            String userId;
            try{
                JSONObject jsonObject = new JSONObject(result.fullResult);
                if (jsonObject.has("userId")) {
                    userId  = jsonObject.getString("userId");
                    SharedPrefs.saveUserId(context, userId);
                    callback.verificationIsDone(true);
                }else
                    callback.verificationIsDone(false);
            }catch (JSONException e){
                e.printStackTrace();
            }
        } else
            UI.showToast("Verification code entered is wrong. \n Please check and try again.", context);
    }

    public interface CodeServiceCallback {
            void verificationIsDone(boolean isOk);
        }
}
